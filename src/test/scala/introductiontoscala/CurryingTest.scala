package introductiontoscala

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.Matchers

@RunWith(classOf[JUnitRunner])
class CurryingTest extends FunSuite with Matchers {

  /**
   * 
   * 
   */
  test("Currying test") {
    
    //having a function line
    def line(a: Int, b: Int, x: Int): Int = a * x + b
    
    //write it's curried version
   //def curriedLine
    
    //write the version of curriedLine function with two default parameters: a = 1 and b = 0
    //def defaultLine
    
    //UNCOMMENT LINES BELOW
    
    //assert(curriedLine(1)(0)(3) == 3)
    //assert(defaultLine(3) == 3)
        
    
  }

}