package introductiontoscala

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.Matchers

@RunWith(classOf[JUnitRunner])
class RecursiveFunctionsTest extends FunSuite with Matchers {

  test("Write a recursive function calculating factorial of value n") {
    
    //uncomment and make it work
    //def fact(x: Int): Int = 
    
    //ASSERTIONS - uncomment them
    //assert(fact(3) == 6)
    //assert(fact(6) == 720)
    //assert(fact(8) == 40320)
  }
  
  test("Sum of înts between a inclusive and b inclusive") {

    //here is a standard method for summing elements between a and b, inclusive
    def standardSum(a: Int, b: Int): Int = {
      if (a > b) 0
      else {
        var result = 0
        for (number <- a to b)
          result += number
        result
      }
    }
    
    assert(standardSum(0, 10) == 55)

    
    //write a recursive version of aforementioned standardSum function
    def recursiveSum(a: Int, b: Int): Int = {

      //update body of this method
      a

    }

    //make this assertion work
    assert(recursiveSum(0,10) == 55)

  }

  /**
   * Write the sum of cubes of all integers between a and b:
   */
  test("Sum of cubes") {

    def cube(x: Int): Int = x * x * x

    def sumCubes(a: Int, b: Int): Int = {

      //update body of this method

      a
    }

    assert(sumCubes(1, 5) == 225)

  }

  test("sum of factorials") {

    //use factorial method which u wrote in the first test in this class
    //def fact(x: Int): Int = 

    def sumFactorials(a: Int, b: Int): Int = {
      //update body of this class
      a
    }

  }

}
