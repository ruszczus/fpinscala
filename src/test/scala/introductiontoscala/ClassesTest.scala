package introductiontoscala

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.Matchers

@RunWith(classOf[JUnitRunner])
class ClassesTest extends FunSuite with Matchers {

  /**
   * Fix the test, that the assertions are correct
   * Use the file in src/main/scala/introductiontoscala/Time.scala
   */
  test("Class Time test") {

    val time = new Time(1, 30)

    //UNCOMMENT THESE ASSERTIONS
    //assert(time.before(new Time(2, 30)))
    //assert(!time.before(new Time(1, 10)))

  }

   /**
   * Fix the test, that the assertions are correct.
   * Use the file in src/main/scala/introductiontoscala/Money.scala
   */
  test("Class Money test") {

    val tenDollars = Money(10)

    assert(tenDollars.amount == 10)
    assert(tenDollars.currency == "USD")
    
    val fifteenEuro = tenDollars.copy(15, "EUR")
    
    assert(fifteenEuro.amount == 15)
    assert(fifteenEuro.currency == "EUR")
    
    //UNCOMMENT THESE ASSERTIONS
    //assert(tenDollars.+(fifteenEuro) == Money(25, "USD"))
    //assert((Money(12) + Money(32)).amount == 44)
  }

}