package introductiontoscala

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.Matchers
import scala.util.Random

@RunWith(classOf[JUnitRunner])
class OperationsOnCollectionsTest extends FunSuite with Matchers {

  /**
   * Set up a map of prices for a number of gadgets that you want to have. Then produce a second map with the same keys and the prices at a 10 percent discount.
   * HINT: use yield
   *
   */
  test("Gadgets with discount") {

    //use this method to print your map
    def prn(x: TraversableOnce[_]) = println(x.mkString(x.getClass.getSimpleName + "(", ", ", ")"))

    val gadgets = Map("iPad3" -> 700, "iPhone 5" -> 600, "MacBook Pro Retina" -> 2000) //a map of your gadgets where key = gadget name, value = price

    //val discount - map of aforementioned gadgets with 10% discount

    
    prn(gadgets)
    //uncomment this line and check the results
    //prn(discount)

  }

  /**
   * Create an array from the array (1,2,3,4,5,6,7,8,9,10) which contains only even values – using yield, and a guard.
   * Use prn method to print the result
   */
  test("Only even values array") {

    //use this method to print your map
    def prn(x: TraversableOnce[_]) = println(x.mkString(x.getClass.getSimpleName + "(", ", ", ")"))

    val array = Array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)

    
    //val result = 

    //prn(result)

    //ASSERTIONS - uncomment them and make them work
    /*	
    result should have size (5)
    result should contain(2)
    result should contain(4)
    result should contain(6)
    result should contain(8)
    result should contain(10)
    */
  }

}