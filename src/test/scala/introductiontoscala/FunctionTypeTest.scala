package introductiontoscala

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.Matchers

@RunWith(classOf[JUnitRunner])
class FunctionTypeTest extends FunSuite with Matchers {

    /**
   * 
   */
  test("Function type test 1") {
    
    //The following code declares an anonymous function that takes a String argument and returns a String:	
    //(s: String) => { prefix + " " + s }
    
    //return this anonymous function  from the body of another function 'saySomething' which takes a prefix value as a parameter
   //def saySomething
    
    // assign that resulting function to a variable sayHello. The saySomething function requires a String argument, so give it one as you create the resulting function sayHello:
    
    
    //call sayHello with a parameter
    //sayHello("Al")
  
    //uncomment below assertion and make it work
    //assert(sayHello("Al") == "Hello Al")   
  
  }

}
