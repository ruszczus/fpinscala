package introductiontoscala

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.Matchers

@RunWith(classOf[JUnitRunner])
class CallByNameTest extends FunSuite with Matchers {

  /**
   * 
   */
  test("Functions call by name test 1") {
   
    //you have a very simple function given
    def time(): Long = {
      println("In time()")
      System.nanoTime
    }

    //here is an example of function with call-by-value parameter
    def execCallByValue(t: Long): Long = {
      println("Entered execCallByValue, calling t ...")
      println("t = " + t)
      println("Calling t again ...")
      t
    }
    
    println(execCallByValue(time()))
    
    
    //write the execCallByName function with the same body but a call-by-name parameter and run it
    //look at the difference in results
    
     //def execCallByName...
    
    //println(execCallByName(time()))
  }

}
