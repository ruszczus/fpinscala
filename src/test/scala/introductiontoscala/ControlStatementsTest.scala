package introductiontoscala

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.Matchers

@RunWith(classOf[JUnitRunner])
class ControlStatementsTest extends FunSuite with Matchers {

  /**
   * Write a Scala equivalent for the Java loop for (int i = 10; i >= 0; i--) System.out.println(i);
   * 
   */
  test("Equivalent for java loop") {
    

  }
  
  

   /**
   * Write a code snippet that prints random numbers using for loop with 'to' and 'until'
   * For printing random number use the method scala.util.Random.nextInt - check Scala spec for more info
   */
  test("Print 10 random numbers ") {
   
   
  }

}