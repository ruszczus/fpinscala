package introductiontoscala

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.Matchers
import scala.util.Random

@RunWith(classOf[JUnitRunner])
class FunctionalCombinatorsTest extends FunSuite with Matchers {

  /**
   * Create a list of string elements out of given list, which contains these elements wrapped in <div></div> block
   *
   */
  test("Strings wrapping") {

    val strings = List("element 1", "element 2", "element 3")

    //modify this line below that the assertions won't fail
    val wrappedStrings = strings


    assert(wrappedStrings(0) == "<div>element 1</div>")
    assert(wrappedStrings(1) == "<div>element 2</div>")
    assert(wrappedStrings(2) == "<div>element 3</div>")
  }
  
  /**
   * Given an array of integers, produce a new array that contains all positive 
   * values of the original array, in their original order, followed by all values that
   * are zero or negative, in their original order.
   *
   */
  test("negative positive order") {
    
    //def orderByPositive

    val a = Array(11, 3, -10, 21, 22, -34, 0, -10, -40, 56, -32)
    
    //UNCOMMENT LINES BELOW
    //val result = orderByPositive(a)

   // println(a.mkString(", "))
   // println(result.mkString(", "))
  }

}