package introductiontoscala

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.Matchers

@RunWith(classOf[JUnitRunner])
class HigherOrderFunctionsTest extends FunSuite with Matchers {

  test("Sum of înts between a inclusive and b inclusive") {

    //you have tree functions defined
    def id(x: Int) = x
    def cube(x: Int): Int = x * x * x
    def fact(x: Int): Int = if (x == 0) 1 else x * fact(x - 1)

    //write a function sum which will make the examples below function correctly

    //def sum

    //EXAMPLES - uncomment them and make them work
    //def sumInts(a: Int, b: Int) = sum(id, a, b)
    //def sumCubes(a: Int, b: Int) = sum(cube, a, b)
    //def sumFactorials(a: Int, b: Int) = sum(fact, a, b)

    //ASSERTIONS - uncomment them and make them work
    //assert(sumInts(1, 10) == 55)
    //assert(sumCubes(1, 10) == 3025)
    //assert(sumFactorials(1, 10) == 4037913)

    //call function sum using anonymous functions instead of function name

    //EXAMPLES - uncomment them and make them work
    //def sumInts2(a: Int, b: Int) = 
    //def sumCubes2(a: Int, b: Int) = 
    //def sumFactorials2(a: Int, b: Int) = 

    //ASSERTIONS - uncomment them and make them work
    //assert(sumInts2(1, 10) == 55)
    //assert(sumCubes2(1, 10) == 3025)
    //assert(sumFactorials2(1, 10) == 4037913)
  }

}
