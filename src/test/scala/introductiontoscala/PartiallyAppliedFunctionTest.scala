package introductiontoscala

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.Matchers

@RunWith(classOf[JUnitRunner])
class PartiallyAppliedFunctionTest extends FunSuite with Matchers {

  /**
   * 
   */
  test("Partially applied function test") {
   
   //there is a simple multiply function
    val multiply = (a: Int, b: Int) => a * b

    //write a partially applied versions of the aforementioned function

    //uncomment these lines and make them work
    //val multiplyBy2 = 
    //val multiplyBy3 =
    
    //assert(multiplyBy2(3) == 6)
    //assert(multiplyBy3(2) == 6)
    
  }
  
   /**
   * 
   */
  test("Partially applied function test 2") {
   
    //having a simple function sending emails:
    def email(to:String, from:String) = {
      "Sending email from: " + from + " to: " + to
    }

    //write a partially applied version which sends email always to the customer support - customersupport@enterprise.com 

    //uncomment this line and make this work
    //def emailCustomerSupport = 
    
    //ASSERTION - uncomment and make it work
    //assert(emailCustomerSupport("CUSTOMER1") == "Sending email from: CUSTOMER1 to: customersupport@enterprise.com")
  }
  
  
}
