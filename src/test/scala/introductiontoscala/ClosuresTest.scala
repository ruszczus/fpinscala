package introductiontoscala

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.Matchers

@RunWith(classOf[JUnitRunner])
class ClosuresTest extends FunSuite with Matchers {

  /**
   * Write a function - belowFirst - which takes a list as a parameter and
   * returns a list of elements which are of lower value than the first element's value 
   * of the list given in the parameter
   * The function signature: belowFirst(xs: List[Int]) => List[Int]
   */
  test("Closures test") {

    val belowFirst  =  ( xs : List[Int] )  =>  {
     
     //modify body of this function
      
      xs
    
    }

    val toTest = List(4,4,5,3,6,2,1,0,4,9,6,8,3);
    val result = belowFirst(toTest)
    
    result should have length 5
    result should not contain oneOf(4 ,6, 9, 8)
    
  }

}