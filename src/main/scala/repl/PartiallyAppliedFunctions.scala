package repl

object PartiallyAppliedFunctions {
  
  def main(args: Array[String]) {
    
    println(wrapWithDiv("Some content"))
  }

  def wrap(prefix: String, html: String, suffix: String) = {
    prefix + html + suffix
  }
  
  val wrapWithDiv = wrap("<div>", _: String, "</div>")
  
}