package repl

// You are not permitted to use these List methods:
// * length
// * map
// * filter
// * ::: (and variations such as ++)
// * flatten
// * flatMap
// * reverse (and variations i.e. reverseMap, reverse_:::)
// This also means you are not permitted to use for-comprehensions on Lists.
// You are permitted to use the functions you write yourself. For example, Exercise 2 may use Exercise 1 or Exercise 3.
// Using permitted existing methods where appropriate will attract marks for elegance.

object Exercises {
  
  def succ(n: Int) = n + 1
  def pred(n: Int) = n - 1
  
  def add(x: Int, y: Int): Int = {
    
    var result = x;
    
    for(i <- 0 until y)  {    
      result = succ(result)
    }
    
    result
  }
  
   def sum(x: List[Int]): Int = {
     3
     
   }
  
   def main(args: Array[String]): Unit = {
     
    println( "Result: " + add(3, 4))
    println( "Result: " + add(7, 8))
  }

}