package repl

object NestedFunctions {

  def main(args: Array[String]) {
    println(max(b = 5, a = 7, c = 4));

  }

  def max(a: Int, b: Int, c: Int) = {

    def max(x: Int, y: Int) = if (x > y) x else y

    max(a, max(b, c))
  }

  def someComplexFunction(parameter: String): String = {
    
    def theFirstStep = {
      println("First step: " + parameter)
      parameter
    }
    
    def theSecondStep = {
      println("Second step: " + parameter)
      parameter
    }

    theFirstStep + theSecondStep
  }

}