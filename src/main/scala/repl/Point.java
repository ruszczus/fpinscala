package repl;

public class Point {

	Integer x;
	Integer y;
	Integer z;
	
	
	public Point(Integer x, Integer y, Integer z) {
		super();
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public Integer getX() {
		return x;
	}
	public Integer getY() {
		return y;
	}
	public Integer getZ() {
		return z;
	}
	
	
}
